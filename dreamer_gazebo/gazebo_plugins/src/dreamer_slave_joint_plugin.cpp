#include <boost/bind.hpp>
#include <stdio.h>
#include <math.h>
#include <vector>
#include <map>
#include <stdexcept>

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <gazebo/math/Angle.hh>

#include "ros/ros.h"
#include "ros/time.h"
#include "std_msgs/Header.h"
#include "std_msgs/Float64.h"

#include <XmlRpcValue.h>
#include <XmlRpcException.h>

#define JOINT_STATE_PUBLISH_FREQ_HZ 400
#define TOLERANCE 1e-6

// namespace gazebo {
//   namespace common {
//     #define gzout (gazebo::common::Console::Instance()->ColorErr("Dbg",__FILE__, __LINE__,36))
//     #define gzprint (gazebo::common::Console::Instance()->ColorErr("FYI:",__FILE__, __LINE__,25))
//   }
// }

namespace gazebo {
  class DreamerSlaveJointPlugin : public ModelPlugin
  {

    public: DreamerSlaveJointPlugin()
    {
      // Start up ROS
      std::string name = "dreamer_slave_joint_plugin";
      int argc = 0;
      ros::init(argc, NULL, name);

      lastPubTime = ros::Time::now();

      MASTER_JOINT_NAME = "m3joint_mt3_j1";
      SLAVE_JOINT_NAME = "m3joint_slave_mt3_j2";
    }

    public: ~DreamerSlaveJointPlugin()
    {
      delete this->node;
      // delete[] this->JOINT_NAMES;
    }

    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
    {
      // Store the pointer to the model
      this->model = _parent;

      // TODO : CHECK base joints are in all joints!!!
      slaveJnt = this->model->GetJoint(SLAVE_JOINT_NAME);
      masterJnt = this->model->GetJoint(MASTER_JOINT_NAME);

      kp = 100;
      kd = 30;

      // ROS Nodehandle
      this->node = new ros::NodeHandle;

      // Listeners for published updates to embedded controller
      this->kpSub = this->node->subscribe("dreamer_slave_joint_kp", 1000, &DreamerSlaveJointPlugin::KpCallback, this);
      this->kdSub = this->node->subscribe("dreamer_slave_joint_kd", 1000, &DreamerSlaveJointPlugin::KdCallback, this);

      // Listen to the update event. This event is broadcast every
      // simulation iteration.
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          boost::bind(&DreamerSlaveJointPlugin::onUpdate, this));
    }

    public: void KpCallback(const std_msgs::Float64 gain_msg)
    {
        kp = gain_msg.data;
        gzdbg << "slave joint Kp callback\n";
    }

    public: void KdCallback(const std_msgs::Float64 gain_msg)
    {
        kd = gain_msg.data;
        gzdbg << "slave joint Kd callback\n";
    }

    /*!
     * Periodically called by Gazebo during each cycle of the simulation.
     */
    public: void onUpdate()
    {
      // Process pending ROS messages, etc.
      ros::spinOnce();

      double masterAngle = masterJnt->GetAngle(0).Radian();
      double masterVelocity = masterJnt->GetVelocity(0);

      double slaveAngle = slaveJnt->GetAngle(0).Radian();
      double slaveVelocity = slaveJnt->GetVelocity(0);

      double slaveTorque = kp * (masterAngle - slaveAngle) + kd * (masterVelocity - slaveVelocity);

      slaveJnt->SetForce(0, slaveTorque);
    }

    // Pointer to the model
    private: physics::ModelPtr model;

    // Pointer to the update event connection
    private: event::ConnectionPtr updateConnection;

    // ROS Nodehandle
    private: ros::NodeHandle * node;

  private:
    // Embedded control parameters...PD loop mimics drive train
    double kp;
    double kd;

    // Subscribers to ros topics for hand controls
    ros::Subscriber kpSub;
    ros::Subscriber kdSub;

    // The time when the robot's state was last published
    ros::Time lastPubTime;

    // Hard code the joint names.  TODO: Obtain this via a ROS service call to the controller.
    std::string SLAVE_JOINT_NAME;
    std::string MASTER_JOINT_NAME;

    physics::JointPtr slaveJnt;
    physics::JointPtr masterJnt;

  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(DreamerSlaveJointPlugin)
}
