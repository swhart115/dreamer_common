#! /usr/bin/env python

import roslib; roslib.load_manifest('dreamer_gazebo')
import rospy
import actionlib
import math
import random

from control_msgs.msg import *
from trajectory_msgs.msg import *
from sensor_msgs.msg import JointState
from copy import copy, deepcopy

TORAD = math.pi/180.0
TODEG = 1.0/TORAD

class dreamerReadyPose :

    def __init__(self, wp, names, prefix):

        print "dreamerReadyPose::init()"

        N = len(names)
        self.jointNames = names
        self.currentData = None
        self.desiredData = None
        self.deadlineData = None

        self.currentState = JointState()
        self.currentState.position = [0]*N
        self.currentState.velocity = [0]*N
        self.currentState.effort = [0]*N
        self.numJoints = N
        self.waypoints = wp

        rospy.Subscriber("dreamer/joint_states", JointState, self.jointStateCallback)

        pub_str = str(prefix + '/command')
        act_str = str(prefix + '/follow_joint_trajectory')

        print "dreamerReadyPose::creating publisher: ", pub_str
        self.trajPublisher = rospy.Publisher(pub_str, JointTrajectory)
        print "dreamerReadyPose::creating action client: ", act_str
        self.trajClient = actionlib.SimpleActionClient(act_str, FollowJointTrajectoryAction)
        print "dreamerReadyPose::creating waiting for server"
        self.trajClient.wait_for_server()

        print "dreamerReadyPose::creating creating goal"

        self.actionGoal = FollowJointTrajectoryGoal()


    def getNumJoints(self) :
        return self.numJoints

    def jointStateCallback(self, data):
        self.currentState = data

    def computeTrajectory(self, desiredData, deadline):

        jointTraj = JointTrajectory()
        currentState = copy(self.currentState)
        desiredState = copy(desiredData)

        # create simple lists of both current and desired positions, based on provided desired names
        rospy.loginfo("dreamerReadyPose::computeTrajectory() -- finding necessary joints")
        desiredPositions = []
        currentPositions = []
        for desIndex in range(len(desiredState.name)) :
            for curIndex in range(len(currentState.name)) :
                if ( desiredState.name[desIndex] == currentState.name[curIndex] ) :
                    desiredPositions.append(desiredState.position[desIndex])
                    currentPositions.append(currentState.position[curIndex])

        rospy.loginfo("dreamerReadyPose::computeTrajectory() -- creating trajectory")
        jointTraj.joint_names = desiredState.name
        jointTraj.points = list()

        for j in range(self.waypoints) :
            trajPoint = JointTrajectoryPoint()

            t = (deadline / self.waypoints) * (j + 1)
            trajPoint.time_from_start = rospy.Duration(t)

            trajPoint.positions = list()
            for i in range(len(desiredPositions)) :
                trajPoint.positions.append( self.minJerk(currentPositions[i], desiredPositions[i], deadline, t) )

            jointTraj.points.append(trajPoint)

        rospy.loginfo("dreamerReadyPose::moveToGoal() -- using tolerances")

        print "dreamerReadyPose::computeTrajectory()"

        return jointTraj


    def minJerk(self, start, end, duration, t):
        tOverD = float(t) / float(duration)
        return start + (end - start)*( 10*math.pow(tOverD,3) - 15*math.pow(tOverD,4) + 6*math.pow(tOverD,5) )

    def moveToGoal(self, jointGoal, deadline, useTolerances) :

        self.actionGoal.trajectory = self.computeTrajectory(jointGoal, deadline)

        offset = 0

        if useTolerances :
            rospy.loginfo("dreamerReadyPose::moveToGoal() -- using tolerances")
            self.actionGoal.path_tolerance = []
            self.actionGoal.goal_tolerance = []

            for k in range(self.numJoints):
                tol.name = self.jointNames[k]
                tol.position = 0.2
                tol.velocity = 1
                tol.acceleration = 10
                self.actionGoal.path_tolerance.append(tol)
                self.actionGoal.goal_tolerance.append(tol)
        else :
            rospy.loginfo("dreamerReadyPose::moveToGoal() -- not using tolerances")

        self.actionGoal.goal_time_tolerance = rospy.Duration(10.0)

        # send goal nad monitor response
        self.trajClient.send_goal(self.actionGoal)

        rospy.loginfo("dreamerReadyPose::moveToGoal() -- returned state: %s", str(self.trajClient.get_state()))
        rospy.loginfo("dreamerReadyPose::moveToGoal() -- returned result: %s", str(self.trajClient.get_result()))

        print "dreamerReadyPose::moveToGoal()"

        return

    def formatJointStateMsg(self, j, offset) :

        if not (len(j) == self.numJoints) :
            rospy.logerr("dreamerReadyPose::formatJointStateMsg() -- incorrectly sized joint message")
            return None

        js = JointState()
        js.header.seq = 0
        js.header.stamp = rospy.Time.now()
        js.header.frame_id = ""
        js.name = []
        js.position = []

        for i in range(self.numJoints):
            js.name.append(self.jointNames[i])
            js.position.append(j[i])

        print "dreamerReadyPose::formatJointStateMsg()"

        return js


if __name__ == '__main__':
    rospy.init_node('dreamer_ready_pose')
    try:

        left_arm_joint_names = ['left_arm_elbow', 'left_arm_elbow_rotator', 'left_arm_pan', 'left_shoulder_pitch', 'left_shoulder_roll', 'left_wrist_j0', 'left_wrist_j1']
        right_arm_joint_names = ['m3joint_ma10_j0', 'm3joint_ma10_j1', 'm3joint_ma10_j2', 'm3joint_ma10_j3', 'm3joint_ma10_j4', 'm3joint_ma10_j5', 'm3joint_ma10_j6']
        head_joint_names = ['m3joint_ms2_j0', 'm3joint_ms2_j1', 'm3joint_ms2_j2', 'm3joint_ms2_j3']
        eye_joint_names = ['m3joint_ms2_j4', 'm3joint_ms2_j5', 'm3joint_ms2_j6']
        torso_joint_names = ['m3joint_mt3_j0', 'm3joint_mt3_j1', 'm3joint_slave_mt3_j2']
        left_hand_joint_names = ['m3joint_ua_left_hand_j0', 'm3joint_ua_left_hand_j1', 'm3joint_ua_left_hand_j2', 'm3joint_ua_left_hand_j3', 'm3joint_ua_left_hand_j4', 'm3joint_ua_left_hand_j5', 'm3joint_ua_left_hand_j6', 'm3joint_ua_left_hand_j7', 'm3joint_ua_left_hand_j8', 'm3joint_ua_left_hand_j9', 'm3joint_ua_left_hand_j10', 'm3joint_ua_left_hand_j11']
        right_hand_joint_names = ['m3joint_ua_mh8_j0', 'm3joint_ua_mh8_j1', 'm3joint_ua_mh8_j2', 'm3joint_ua_mh8_j3', 'm3joint_ua_mh8_j4', 'm3joint_ua_mh8_j5', 'm3joint_ua_mh8_j6', 'm3joint_ua_mh8_j7', 'm3joint_ua_mh8_j8', 'm3joint_ua_mh8_j9', 'm3joint_ua_mh8_j10', 'm3joint_ua_mh8_j11']

        dreamerTrajectoryGeneratorLeft = dreamerReadyPose(100, left_arm_joint_names,'/dreamer/left_arm_controller')
        dreamerTrajectoryGeneratorRight = dreamerReadyPose(100, right_arm_joint_names,'/dreamer/right_arm_controller')
        dreamerTrajectoryGeneratorHead = dreamerReadyPose(100, head_joint_names,'/dreamer/head_controller')
        dreamerTrajectoryGeneratorEyes = dreamerReadyPose(100, eye_joint_names,'/dreamer/eye_controller')
        dreamerTrajectoryGeneratorLeftHand = dreamerReadyPose(10, left_hand_joint_names,'/dreamer/left_hand_controller')
        dreamerTrajectoryGeneratorRightHand = dreamerReadyPose(10, right_hand_joint_names,'/dreamer/right_hand_controller')
        dreamerTrajectoryGeneratorTorso = dreamerReadyPose(100, torso_joint_names,'/dreamer/torso_controller')
        rospy.sleep(2)

        lrp = [0.0*TORAD, 0.0*TORAD, 0.0*TORAD, 0.0*TORAD, 0.0*TORAD, 0.0*TORAD, 0.0*TORAD]
        rrp = [0.0*TORAD, 0.0*TORAD, 0.0*TORAD, 0.0*TORAD, 0.0*TORAD, 0.0*TORAD, 0.0*TORAD]
        hrp = [0.0*TORAD, 0.0*TORAD, 0.0*TORAD, 0.0*TORAD]
        erp = [0.0*TORAD, 0.0*TORAD, 0.0*TORAD]
        trp = [0.0*TORAD, 0.0*TORAD, 0.0*TORAD]
        lhrp = [0]*12
        rhrp = [0]*12

        print "dreamerReadyPose() -- moving to ready pose"

        jointGoalLeft = dreamerTrajectoryGeneratorLeft.formatJointStateMsg(lrp, 0)
        jointGoalRight = dreamerTrajectoryGeneratorRight.formatJointStateMsg(rrp, 0)
        jointGoalHead = dreamerTrajectoryGeneratorHead.formatJointStateMsg(hrp, 0)
        jointGoalEyes = dreamerTrajectoryGeneratorEyes.formatJointStateMsg(erp, 0)
        jointGoalLeftHand = dreamerTrajectoryGeneratorLeftHand.formatJointStateMsg(lhrp, 0)
        jointGoalRightHand = dreamerTrajectoryGeneratorRightHand.formatJointStateMsg(rhrp, 0)
        jointGoalTorso = dreamerTrajectoryGeneratorTorso.formatJointStateMsg(trp, 0)
        dreamerTrajectoryGeneratorLeft.moveToGoal(jointGoalLeft, 0.5, False)
        dreamerTrajectoryGeneratorRight.moveToGoal(jointGoalRight, 0.5, False)
        dreamerTrajectoryGeneratorLeftHand.moveToGoal(jointGoalLeftHand, 0.1, False)
        dreamerTrajectoryGeneratorRightHand.moveToGoal(jointGoalRightHand, 0.1, False)
        dreamerTrajectoryGeneratorTorso.moveToGoal(jointGoalTorso, 0.5, False)
        dreamerTrajectoryGeneratorHead.moveToGoal(jointGoalHead, 0.5, False)
        dreamerTrajectoryGeneratorEyes.moveToGoal(jointGoalEyes, 0.5, False)

    except rospy.ROSInterruptException:
        pass




